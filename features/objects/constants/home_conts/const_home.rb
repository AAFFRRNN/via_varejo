# !/usr/bin/env ruby

class HomeConst

	FIELD_SEARCH = "strBusca"
	BTN_SEARCH = "btnOK"
	BTN_CAR = "btnAdicionarCarrinho"
	BTN_CONTINUE = "btnContinuar"
	BTN_LOGOUT = "Sair"
	HREF_IPHONE = "https://www.casasbahia-imagens.com.br/TelefoneseCelulares/Smartphones/iPhone/15253178/1114527869/iphone-xr-apple-preto-64gb-tela-retina-lcd-de-61-ios-12-camera-traseira-12mp-resistente-a-agua-e-reconhecimento-facial-15253178.jpg"
	HREF_HOME = "https://www.casasbahia.com.br/?_ga=2.192203554.906902299.1593989976-270380546.1593989976"
	TEXT_CAR = "Meu Carrinho"
	TEXT_IPHONE = "iPhone XR Apple Preto 64GB, Tela Retina LCD de 6,1”, iOS 12, Câmera Traseira 12MP, Resistente à Água e Reconhecimento Facial"
end