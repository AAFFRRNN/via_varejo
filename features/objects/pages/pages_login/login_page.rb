# !/usr/bin/env ruby

class LoginPage

	def self.click_entrar
		ClickItems.click_identification(LoginConst::BTN_ENTRAR)
	end

 	def self.insert_email(text)
 		InsertData.insert_id(LoginConst::FIELD_EMAIL, text)
 	end 

	def self.insert_password(text)
 		InsertData.insert_id(LoginConst::FIELD_PASSWORD, text)
 	end

 	def self.click_continue
 		ClickItems.click_identification(LoginConst::BTN_CONTINUE)
 	end
end