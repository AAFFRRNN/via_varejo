# !/usr/bin/env ruby

class HomePage

 	def self.insert_search(text)
 		InsertData.insert_id(HomeConst::FIELD_SEARCH, text)
 	end 

 	def self.click_search
 		ClickItems.click_identification(HomeConst::BTN_SEARCH)
 	end

 	def self.click_iphone
 		ClickItems.click_href(HomeConst::HREF_IPHONE)
 	end

 	def self.click_car
 		ClickItems.click_identification(HomeConst::BTN_CAR)
 	end

 	def self.click_continue
 		ClickItems.click_identification(HomeConst::BTN_CONTINUE)
 	end

 	def self.click_home
 		ClickItems.click_href(HomeConst::HREF_HOME)
 	end

 	def self.click_logout
 		ClickItems.click_href(HomeConst::BTN_LOGOUT)
 	end
end