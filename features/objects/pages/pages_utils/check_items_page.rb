# !/usr/bin/env ruby

class CheckItems

	def self.check_text(text)
		text.split("|").each do |field|
			expect(page).to have_content(field.strip)
		end
	end

	def self.check_text_h2(text)
		text.split("|").each do |field|
			expect(page).to have_selector('h2', text: field.strip)
		end
	end
end