# !/usr/bin/env ruby

class ClickItems
	
	def self.click_identification(identification)
 		identification.split("|").each do |field|
 			page.find(:id, field.strip).click
 		end
 	end
	
 	def self.click_href(href)
 		href.split("|").each do |field|
 			page.find(:xpath, "//*[@href='#{field.strip}']").click
 		end 
 	end
end