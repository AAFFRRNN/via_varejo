# !/usr/bin/env ruby

class InsertData
	
 	def self.insert_id(identification, text)
 		page.fill_in(identification, with: text)
 	end
end