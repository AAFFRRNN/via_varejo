# !/usr/bin/env ruby

class OpenPage

	def self.open_url(site)
		visit(site)
	end
end