@iphone
Feature: Adicionar produto ao carrinho   
 Como cliente das Casas Bahia
 Quero adicionar um produto no carrinho    
 Para reservar meu produto   

Scenario: Adicionar um Iphone XR no carrinho 
	Given I am on the site Casa Bahia
	When I access the store using a valid account
		And I look for the Iphone XR and add my car
	Then I see my Iphone in car
	When I logout Casa Bahia