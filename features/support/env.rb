require 'capybara/cucumber'
require 'selenium-webdriver'
require 'rubygems'
require 'bundler/setup'
require 'capybara'
require 'capybara/rspec'
require 'capybara/dsl'
require 'capybara/rspec/matchers'
require 'capybara/rspec/matcher_proxies'
require 'ffaker'
require 'faker'
require 'cpf_faker'
require 'rspec'
require 'headless'
require 'xvfb'
require 'ostruct'
require 'httparty'
require 'httparty/request'
require 'httparty/response/headers'
require 'date'
require 'active_support/time'
require 'time'


require_relative 'helper.rb'

include Capybara::DSL
include Capybara::RSpecMatchers
include RSpec::Matchers

ENVIRONMENT_TYPE = ENV['ENVIRONMENT_TYPE']

BROWSER = ENV['BROWSER']

CONFIG = YAML.load_file(File
  .dirname(__FILE__) + "/data/#{ENVIRONMENT_TYPE}.yml")

Capybara.register_driver :selenium do |app|
  if BROWSER.eql?('chrome')
    Capybara::Selenium::Driver.new(
      app,
      browser: :chrome,
      desired_capabilities: Selenium::WebDriver::Remote::Capabilities.chrome(
        chromeOptions: { 
          args: %w[ window-size=1280,1024]
        }
      )
    )
  elsif BROWSER.eql?('firefox')
    Capybara::Selenium::Driver.new(
      app,
      browser: :firefox,
      desired_capabilities: Selenium::WebDriver::Remote::Capabilities
        .firefox(marionette: true)
    )
  elsif BROWSER.eql?('headless')

	caps = Selenium::WebDriver::Remote::Capabilities.chrome(
		chromeOptions: {
			args: %w[ no-sandbox headless disable-gpu window-size=1280,1024]
		}
	)

	Capybara::Selenium::Driver.new(app, browser: :chrome, desired_capabilities: caps)
  end
end

Capybara.configure do |config|
  config.default_driver = :selenium
  config.app_host = CONFIG['url_home']
end

Capybara.default_max_wait_time = 10
Capybara.javascript_driver = :webkit

After do |scenario|
  Capybara.reset_sessions!
end