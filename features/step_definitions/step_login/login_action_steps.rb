# !/usr/bin/env ruby

When(/^I access the store using a valid account$/) do
	LoginAction.system_access_email(DataConst::EMAIL_VALID, DataConst::PASSWORD_VALID)
end