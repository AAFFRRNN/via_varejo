# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration - Develop
	Install Ruby version 2.5.0 and set version
		1 - Start command prompt
		2 - Install RMV
			curl -sSL https://get.rvm.io | bash -s stable
		3 - Install Ruby 2.5.0
			rvm install 2.5.0
		4 - List ruby
			rvm list	
		5 - Set Ruby
			4.1 - Set ruby default
				rvm use default 2.5.0
			4.2 - Set ruby current 
				rvm use 2.5.0
	Install Gems
		1 - Start command prompt
		2 - Install GEMS
			gem install builder -v 3.2.3
			gem install bundler -v 1.16.1
			gem install cucumber -v 3.1.0
			gem install capybara -v 2.17.0
			gem install selenium-webdriver -v 3.8.0
			gem install rails -v 5.1.4
			gem install rspec -v 3.7.0
			gem install chromedriver-helper -v 1.1.0
			gem install geckodriver-helper -v 0.0.4
			gem install faker -v 1.8.7
			gem install cpf_faker -v 1.3.0
			gem install headless -v 2.3.1
			gem install xvfb -v 1.0.4
			gem install rake -v 12.3.0
			gem install rspec -v 3.7.0
			gem install httparty -v 0.15.6
			gem install chrome -v 0.1.0
			gem install bundler-unload -v 1.0.2
			gem install dsl -v 0.2.3
			gem install jar_wrapper -v 0.1.8
			gem install rails_current -v 1.9.0
			gem install rails_default_url_options -v 3.0.0
			gem install rails_helper -v 2.2.2
			gem install rainbow -v 3.0.0
			gem install rubocop -v 0.52.1
			gem install rubygems-bundler -v 1.4.4
			gem install selenium -v 0.2.11
			gem install ffaker -v 2.9.0
	Clone Project Via Varejo Automation
		1 - Start command prompt
		2 - Creating one folder
			mkdir <name folder>
		3 - Acessing the folder where the repository will be cloned
			cd ~/<name folder>
		4 - Clone repository
			git clone git@bitbucket.org:AAFFRRNN/via_varejo.git

* Dependencies
* Database configuration
* How to run tests
	Run tests automations
		1 - Acessing the folder the feature is visible from the automation project
		2 - Start automation test
			cucumber
		3 - Start automation test with specific tag and chrome
			cucumber -p pretty -p html_report -p chrome -p staging --tags @iphone
		4 - Start automation test with specific tag and headless
			cucumber -p pretty -p html_report -p headless -p staging --tags @iphone

* Deployment instructions
* Jenkins Configuration
	Creating a job with the setting capybara
		1 - Acess the jenkins
		2 - Installing the plugins below in "Management Plugins" in "Management the Jekins"
			Cucumber reports
			RVM
		3 - Press the item "New Job"
		4 - Insert name the job in field "Enter an item name"
		5 - Select the item "Build a free-style software project"
		6 - Select the item configuration in job created
		7 - In Source Code Management
			7.1 - Select the item "Git"	
			7.2 - In "Repository URL" insert one git address valid
			7.3 - Insert one "Credentials" if necessary
			7.4 - In "Branch Specifier" insert the branch with you code commit
		8 - In Trigger the builds
			8.1 - Select the item Build Periodically
			8.2 - Insert in schedule field the command -> H <time> * * * for start job daily at the stimate time
		9 - In Build Environment
			9.1 - Selected the item "Run the build in a RVM-managed environment"
			9.2 - Insert in field "Implementation" the version ruby installed in environment
		10 - In build
			10.1 - Press the item "Add step in build"
			10.2 - Press 3 times the item "Run Shell"
			10.3 - In first command field command insert the command below
				gem install bundler
				bundle install
			10.4 - In second command field insert the command below
				[ -d reports ] && rm -rf reports
				mkdir reports
			10.5 - In third command field insert the command below
				cucumber -p pretty -p json_report -p headless -p staging
		11 - In Post-build actions
			11.1 - Press the item "Post-build actions"
			11.2 - Select the item "Cucumber Reports"
			11.3 - In "Json Reports Path" field insert the command below
				reports
			11.4 - In "File Include Pattern" field insert the command below
				** /* . json
		12 - Press button "Save"